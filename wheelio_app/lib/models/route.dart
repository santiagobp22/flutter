import 'package:meta/meta.dart' show required;

class Route {
  final String id;
  final DateTime arrivalTime;
  final double duration, length;
  final String polyline;

  Route({
    required this.id,
    required this.arrivalTime,
    required this.duration,
    required this.length,
    required this.polyline,
  });

  static Route fromJson(Map<String, dynamic> json) {
    final section = Map<String, dynamic>.from(json['sections'][0]);

    return Route(
      id: json['id'],
      arrivalTime: DateTime.parse(section['arrival']['time']),
      duration: (section['summary']['duration'] as int).toDouble(),
      length: (section['summary']['length'] as int).toDouble(),
      polyline: section['polyline'],
    );
  }
}
