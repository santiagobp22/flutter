import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:wheelio_app/api/routing_api.dart';
import 'package:wheelio_app/flexible_polyline/flexible_polyline.dart';
import 'package:wheelio_app/models/places.dart';
import 'package:wheelio_app/models/route.dart' as heremaps;
import 'package:wheelio_app/widgets/my_custom_marker.dart';

Future<Uint8List?> loadAsset(
  String path, {
  int width = 50,
  int height = 50,
}) async {
  ByteData data = await rootBundle.load(path);
  final Uint8List bytes = data.buffer.asUint8List();
  final ui.Codec codec = await ui.instantiateImageCodec(
    bytes,
    targetHeight: height,
    targetWidth: width,
  );
  final ui.FrameInfo frame = await codec.getNextFrame();
  ByteData? byteData =
      await frame.image.toByteData(format: ui.ImageByteFormat.png);
  return byteData?.buffer.asUint8List();
}

Future<Uint8List?> loadImageFromNetwork(
  String url, {
  int width = 50,
  int height = 50,
}) async {
  final response = await http.Client().get(Uri.parse(url));
  if (response.statusCode == 200) {
    final Uint8List bytes = response.bodyBytes;
    final ui.Codec codec = await ui.instantiateImageCodec(
      bytes,
      targetHeight: height,
      targetWidth: width,
    );
    final ui.FrameInfo frame = await codec.getNextFrame();
    ByteData? byteData =
        await frame.image.toByteData(format: ui.ImageByteFormat.png);
    return byteData?.buffer.asUint8List();
  }
  throw Exception('No Image Could Be Downloaded');
}

double getCoordsRotation(LatLng currentPosition, LatLng lastPosition) {
  double dx = math.cos(math.pi * lastPosition.latitude / 180) *
      (currentPosition.longitude - lastPosition.longitude);
  double dy = currentPosition.latitude - lastPosition.latitude;

  final angle = math.atan2(dy, dx);

  return 90 - angle * 180 / math.pi;
}

Future<Uint8List?> placeToMarker(Place place,
    {required bool isDeparture, int? duration}) async {
  ui.PictureRecorder recorder = ui.PictureRecorder();
  Canvas canvas = Canvas(recorder);
  final Size size = Size(400, 100);
  MyCustomMarker customMarker = MyCustomMarker(
    place: place,
    isDeparture: isDeparture,
    duration: duration,
  );

  customMarker.paint(canvas, size);

  ui.Picture picture = recorder.endRecording();

  final ui.Image image = await picture.toImage(
    size.width.toInt(),
    size.height.toInt(),
  );

  final ByteData? byteData = await image.toByteData(
    format: ui.ImageByteFormat.png,
  );

  return byteData?.buffer.asUint8List();
}

double getZoom(LatLng arrival, LatLng departure) {
  final linearDistance = getDistanceInKM(departure, arrival);

  double radius = linearDistance / 2;
  double scale = radius / 0.3;
  double zoom = (16 - math.log(scale) / math.log(2));

  return zoom;
}

CameraUpdate centerMap(LatLng arrival, LatLng departure,
    {double padding = 20}) {
  final double left = math.min(arrival.latitude, departure.latitude);
  final double top = math.min(arrival.longitude, departure.longitude);
  final double right = math.max(arrival.latitude, departure.latitude);
  final double bottom = math.max(arrival.longitude, departure.longitude);

  final LatLng southwest = LatLng(left, bottom);
  final LatLng northeast = LatLng(right, top);
  final LatLngBounds bounds = LatLngBounds(
    southwest: southwest,
    northeast: northeast,
  );

  if (Platform.isIOS) {
    final CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(
      bounds,
      padding,
    );
    return cameraUpdate;
  }

  final LatLng latLng = LatLng(
    (southwest.latitude + northeast.latitude) / 2,
    (southwest.longitude + northeast.longitude) / 2,
  );
  final zoom = getZoom(arrival, departure);
  return CameraUpdate.newLatLngZoom(
    latLng,
    zoom - (padding / 400),
  );
}

double deg2rad(deg) {
  return deg * (math.pi / 180);
}

double getDistanceInKM(LatLng position1, LatLng position2) {
  final lat1 = position1.latitude;
  final lon1 = position1.longitude;
  final lat2 = position2.latitude;
  final lon2 = position2.longitude;

  final R = 6371; // Radius of the earth in km
  final dLat = deg2rad(lat2 - lat1); // deg2rad below
  final dLon = deg2rad(lon2 - lon1);
  final a = math.sin(dLat / 2) * math.sin(dLat / 2) +
      math.cos(deg2rad(lat1)) *
          math.cos(deg2rad(lat2)) *
          math.sin(dLon / 2) *
          math.sin(dLon / 2);
  final c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
  final d = R * c; // Distance in km

  return d;
}

Marker createMarker(
    {required String id, required LatLng position, required Uint8List bytes}) {
  final markerId = MarkerId(id);
  final marker = Marker(
    markerId: markerId,
    position: position,
    icon: BitmapDescriptor.fromBytes(
      bytes,
    ),
  );
  return marker;
}

Future<RouteData?> createRoute({
  required Map<PolylineId, Polyline> polylines,
  required LatLng arrival,
  required LatLng departure,
}) async {
  final newPolylines = Map<PolylineId, Polyline>.from(polylines);
  final routes = await RoutingAPI.instance.calculate(departure, arrival);
  if (routes.isNotEmpty) {
    PolylineId polylineId = PolylineId('route');
    final heremaps.Route route = routes[0];
    final tmp = FlexiblePolyline.decode(route.polyline);
    final points = tmp.map((e) => LatLng(e.lat, e.lng)).toList();
    final Polyline polyline = Polyline(
      polylineId: polylineId,
      width: 4,
      color: Colors.blue[900] ?? Colors.blue,
      points: points,
    );
    newPolylines[polylineId] = polyline;
    return RouteData(route, newPolylines);
  }
  return null;
}

class RouteData {
  final heremaps.Route route;
  final Map<PolylineId, Polyline> polylines;

  RouteData(this.route, this.polylines);
}
