
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Database/bloc/database_bloc.dart';
import 'package:wheelio_app/Database/repository/database_repository.dart';

class DriverData extends StatelessWidget {
  final String start;
  final String end;
  final String driverName;
  final String plate;
  final String photo;
  final TextStyle ts = const TextStyle(fontSize: 20, color: Colors.black87);
  final TextStyle tst = const TextStyle(
      fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);

  const DriverData(
      {Key? key,
      required this.start,
      required this.end,
      required this.driverName,
      required this.plate,
      required this.photo})
      : super(key: key);
  

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
        theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Viaje'),
              centerTitle: true, // appbar text center.
            ),
            body: MultiBlocListener(
                listeners: [
                  BlocListener<ConnectivityBloc, ConnectivityState>(
                    listener: (context, state) {
                      // TODO: implement listener
                    },
                  ),
                ],
                child: BlocBuilder<DatabaseBloc, DatabaseState>(

                    builder:  (context, state)  {

                      return
                    SingleChildScrollView(
                        child: Padding(
                  padding: const EdgeInsets.all(35.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          color: Colors.white38,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  flex: 3,
                                  fit: FlexFit.loose,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Inicio:", style: tst),
                                      Text(
                                        start,
                                        style: ts,
                                      ),
                                      SizedBox.fromSize(
                                        size: const Size(1, 12),
                                      ),
                                      Text("Fin:", style: tst),
                                      Text(
                                        end,
                                        style: ts,
                                      ),
                                    ],
                                  )),
                              const Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Icon(
                                    IconData(0xe3c8,
                                        fontFamily: 'MaterialIcons'),
                                    size: 50),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          color: Colors.white38,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  flex: 3,
                                  fit: FlexFit.loose,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Nombre:", style: tst),
                                      Text(
                                        driverName,
                                        style: ts,
                                      ),
                                      SizedBox.fromSize(
                                        size: const Size(1, 12),
                                      ),
                                      Text("Placa:", style: tst),
                                      Text(
                                        plate,
                                        style: ts,
                                      ),
                                    ],
                                  )),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                //TODO cargar imagen
                                child: Container(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ));}))));
  }
}
