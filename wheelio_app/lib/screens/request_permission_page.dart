import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import 'home_page_map.dart';

class RequestPermissionPage extends StatefulWidget {
  static const routeName = 'request-permission';

  RequestPermissionPage({Key? key}) : super(key: key);

  @override
  State<RequestPermissionPage> createState() => _RequestPermissionPageState();
}

class _RequestPermissionPageState extends State<RequestPermissionPage>
    with WidgetsBindingObserver {
  bool _fromSettings = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed && _fromSettings) {
      _check();
    }
  }

  _check() async {
    final bool hasAccess = await Permission.locationWhenInUse.isGranted;
    if (hasAccess) {
      _goToHome();
    }
  }

  _goToHome() {
    Navigator.pushReplacementNamed(context, HomePageMap.routeName);
  }

  _openAppSettingsCustom() async {
    await openAppSettings();
    _fromSettings = true;
  }

  Future<void> _request() async {
    final PermissionStatus status =
        await Permission.locationWhenInUse.request();

    switch (status) {
      case PermissionStatus.denied:
        // TODO: Handle this case.
        if (Platform.isIOS) {
          _openAppSettingsCustom();
        }
        break;
      case PermissionStatus.granted:
        _goToHome();
        break;
      case PermissionStatus.restricted:
        // TODO: Handle this case.
        break;
      case PermissionStatus.limited:
        // TODO: Handle this case.
        break;
      case PermissionStatus.permanentlyDenied:
        // TODO: Handle this case.
        _openAppSettingsCustom();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'ACCESS TO LOCATION REQUIRED',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'We require the location to bring the best carpooling experience',
                textAlign: TextAlign.center,
              ),
            ),
            TextButton(
              onPressed: _request,
              child: const Text('ALLOW'),
            ),
          ],
        ),
      ),
    );
  }
}
