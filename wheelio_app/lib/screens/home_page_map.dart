import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/bloc/screens/home/bloc.dart';
import 'package:wheelio_app/widgets/center_marker.dart';
import 'package:wheelio_app/widgets/custom_app_bar.dart';
import 'package:wheelio_app/widgets/my_location_button.dart';

class HomePageMap extends StatefulWidget {
  static const routeName = 'home-page';

  HomePageMap({Key? key}) : super(key: key);

  @override
  State<HomePageMap> createState() => _HomePageMapState();
}

class _HomePageMapState extends State<HomePageMap> {
  final HomeMapBloc _bloc = HomeMapBloc();

  final Completer<GoogleMapController> _completer = Completer();

  final CameraPosition _initialPosition = const CameraPosition(
    target: LatLng(
      4.6014581,
      -74.0683221,
    ),
    zoom: 14,
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  Future<GoogleMapController> get _mapController async {
    return await _completer.future;
  }

  void setMapController(GoogleMapController controller) {
    _completer.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        appBar: CustomAppBar(),
        body: SafeArea(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: BlocBuilder<HomeMapBloc, HomeMapState>(
              builder: (_, HomeMapState state) {
                if (!state.gpsEnabled) {
                  return const Center(
                    child: Text('Enable GPS!'),
                  );
                }

                if (state.loading) {
                  return const Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.yellow,
                    ),
                  );
                }
                final CameraPosition initialPosition = CameraPosition(
                  target: state.myLocation,
                  zoom: 14,
                );

                return Column(
                  children: [
                    Expanded(
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          GoogleMap(
                            initialCameraPosition: initialPosition,
                            compassEnabled: true,
                            zoomControlsEnabled: false,
                            myLocationEnabled: false,
                            myLocationButtonEnabled: true,
                            markers: state.markers.values.toSet(),
                            polylines: state.polylines.values.toSet(),
                            polygons: state.polygons.values.toSet(),
                            onTap: (LatLng position) {
                              _bloc.add(OnMapTap(position));
                            },
                            onMapCreated: (controller) {
                              _bloc.setMapController(controller);
                            },
                          ),
                          MyLocationButton(),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16),
                      child: SizedBox(
                        width: double.infinity,
                        child: TextButton(
                          child: const Text(
                            '¿A dónde vas?',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.yellowAccent[700],
                            backgroundColor: Colors.blue[900],
                          ),
                          onPressed: () {
                            _bloc.weYouGo(context);
                          },
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
