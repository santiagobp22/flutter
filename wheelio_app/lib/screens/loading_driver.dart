import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'driver_data.dart';

class LoadingDriver extends StatefulWidget {
  String start;
  String end;
  LoadingDriver({Key? key, required this.start, required this.end})
      : super(key: key);

  @override
  State<LoadingDriver> createState() => _StatefulLoadingDriver();
}

class _StatefulLoadingDriver extends State<LoadingDriver> {

  late String driverName;
  late String plate;
  late String photo;
  late Timer timr;
  bool waiting = false;
  Future<bool> tryConnection() async {
    try {
      final response = await InternetAddress.lookup("www.google.com");
      return response.isNotEmpty;
    } on SocketException {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    timr = Timer.periodic(const Duration(seconds: 1), (Timer timer) async {
      final pref = await SharedPreferences.getInstance();
      var res = await http
          .get(Uri.parse('https://desolate-spire-63486.herokuapp.com/status'));
      var js = json.decode(res.body);

      bool b = js["onTrip"];
      driverName = js["data"]["driverName"];
      plate = js["data"]["plate"];
      photo = js["data"]["photo"];

      if (b) {
        await pref.setBool("activeTravel", b);
        await pref.setStringList(
            "travelData", List.of([widget.start, widget.end, driverName, plate, photo]));
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
              builder: (context) => DriverData(
                  start: widget.start,
                  end: widget.end,
                  driverName: driverName,
                  plate: plate,
                  photo: photo)),
        );
        timer.cancel();
      }
    });
    return MaterialApp(
        theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Esperando conductor'),
              centerTitle: true, // appbar text center.
            ),
            body: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Esperando Conductor", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
                        const SizedBox(height:100, width: double.infinity,),
                        const CircularProgressIndicator(),
                        const SizedBox(height:100),
                        SizedBox(
                            width: 170, // <-- Your width
                            height: 50, // <-- Your height
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        const Color(0xFF00468B)),
                              ),
                              onPressed: () async {
                                timr.cancel();
                                Navigator.of(context).pop();
                              },
                              child: const Text("Cancelar viaje"),
                            ))
                      ],
                    )));
  }
}
