import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_oauth/firebase_auth_oauth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class TravelConfirmation extends StatelessWidget {
  TravelConfirmation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
      home: Scaffold(
          appBar: AppBar(
            title: Text('Información del viaje'),
            centerTitle: true, // appbar text center.
          ),
          body: StreamBuilder(
              initialData: null,
              stream: FirebaseAuth.instance.userChanges(),
              builder: (BuildContext context, AsyncSnapshot<User?> snapshot) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 75.0),

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage('assets/images/Logo.PNG')),
                      SizedBox(
                        height: 45,
                      ),
                      Card(
                        color: Colors.lightBlue[200],
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.zero,
                        ),
                        margin: EdgeInsets.all(20.0),
                        child: Container(
                          width: 200,
                          height: 120,
                          child: Center(
                            child: Text(
                              'Tu viaje ha sido confirmado.',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20, color: Colors.black),
                            )
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 55,
                      ),
                      SizedBox(
                          width: 170, // <-- Your width
                          height: 50, // <-- Your height
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF00468B)),
                            ),
                            onPressed: (){},
                            child: Text("Aceptar"),
                          ),
                      )

                    ],
                  ),
                );
              })),
    );
  }
}
