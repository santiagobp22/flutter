import 'package:flutter/material.dart';
import 'package:wheelio_app/models/places.dart';
import 'dart:ui' as ui;

class MyCustomMarker extends CustomPainter {
  final Place place;
  final bool isDeparture;
  final int? duration;

  MyCustomMarker(
      {required this.place, required this.isDeparture, this.duration});

  void _buildMiniRect(Canvas canvas, Paint paint, double size) {
    paint.color = Colors.black;
    final rect = Rect.fromLTWH(0, 0, size, size);
    canvas.drawRect(rect, paint);
  }

  void _buildParagraph({
    required Canvas canvas,
    required List<String> text,
    required double width,
    required Offset offset,
    Color color = Colors.black,
    double fontSize = 34,
    String? fontFamily,
    TextAlign? textAlign = TextAlign.left,
  }) {
    final ui.ParagraphBuilder builder = ui.ParagraphBuilder(
      ui.ParagraphStyle(
        fontSize: fontSize,
        textAlign: textAlign,
      ),
    );

    builder.pushStyle(
      ui.TextStyle(
        color: color,
        fontFamily: fontFamily,
      ),
    );

    if (text.length > 1) {
      builder.pushStyle(
        ui.TextStyle(
          fontWeight: FontWeight.bold,
        ),
      );
    }
    builder.addText(text[0]);
    if (text.length > 1) {
      builder.addText(text[1]);
    }

    final ui.Paragraph paragraph = builder.build();

    paragraph.layout(ui.ParagraphConstraints(width: width));
    canvas.drawParagraph(
        paragraph, Offset(offset.dx + 10, offset.dy - paragraph.height / 2));
  }

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint();
    paint.color = Colors.white;

    final height = size.height - 15;

    final RRect rect = RRect.fromLTRBR(
      0,
      0,
      size.width,
      height,
      Radius.circular(0),
    );

    canvas.drawRRect(rect, paint);

    final Rect stickRect = Rect.fromLTWH(size.width / 2 - 2.5, height, 5, 15);

    canvas.drawRect(stickRect, paint);

    _buildMiniRect(canvas, paint, height);

    if (isDeparture) {
      _buildParagraph(
        canvas: canvas,
        text: [String.fromCharCode(Icons.gps_fixed_outlined.codePoint)],
        width: height,
        offset: Offset(height / 2 - 40, height / 2),
        color: Colors.white,
        fontSize: 60,
        fontFamily: Icons.gps_fixed_outlined.fontFamily,
      );
    } else {
      _buildParagraph(
        canvas: canvas,
        text: ['${duration}\n', 'min'],
        width: 60,
        textAlign: TextAlign.center,
        offset: Offset(height / 2 - 40, height / 2),
        color: Colors.white,
        fontSize: 30,
      );
    }

    _buildParagraph(
      canvas: canvas,
      text: [place.title],
      width: size.width - height - 20,
      offset: Offset(height, height / 2),
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
