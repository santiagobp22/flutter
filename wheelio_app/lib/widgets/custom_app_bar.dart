import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/api/search_api.dart';
import 'package:wheelio_app/bloc/screens/home/bloc.dart';
import 'package:wheelio_app/models/places.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeMapBloc>(context);
    return BlocBuilder<HomeMapBloc, HomeMapState>(
      builder: (_, state) => Container(
        padding: EdgeInsets.all(16),
        child: SafeArea(
          child: Row(
            children: const [
              Text('Wheelio'),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size(
        double.infinity,
        50,
      );
}

class SearchPlacesDelegate extends SearchDelegate<Place> {
  final SearchAPI _api = SearchAPI.instance;
  final List<Place> places;
  final LatLng at;

  SearchPlacesDelegate(this.at, this.places);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          this.query = '';
        },
        icon: Icon(Icons.clear),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(
          context,
          Place(
            id: '',
            title: '',
            vicinity: '',
            position: const LatLng(0.0, 0.0),
          ),
        );
      },
      icon: const Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (this.query.trim().length >= 3) {
      return FutureBuilder<List<Place>>(
        future: _api.searchPlace(query, at),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                final Place place = snapshot.data![index];
                return ListTile(
                  onTap: () {
                    places.add(place);
                    print('Place Chosen: $place');
                    close(context, place);
                  },
                  title: Text(
                    place.title,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(place.vicinity.replaceAll('<br/>', ' - ')),
                );
              },
            );
          } else if (snapshot.hasError) {
            return const Text('ERROR');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    }
    return const Text('Please describe your search better');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Place> history = places;

    if (this.query.trim().length > 0) {
      history = history.where((element) {
        return element.title.toLowerCase().contains(query.toLowerCase());
      }).toList();
    }

    return ListView.builder(
      itemCount: history.length,
      itemBuilder: (context, index) {
        final Place place = history[index];
        return ListTile(
          onTap: () {
            print('Place Chosen: $place');
            close(context, place);
          },
          title: Text(
            place.title,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(place.vicinity.replaceAll('<br/>', ' - ')),
        );
      },
    );
  }
}
