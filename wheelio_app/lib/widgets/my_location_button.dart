import 'package:flutter/material.dart';
import 'package:wheelio_app/bloc/screens/home/bloc.dart';

class MyLocationButton extends StatelessWidget {
  const MyLocationButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = HomeMapBloc.of(context);
    return Positioned(
      bottom: 16,
      right: 16,
      child: FloatingActionButton(
        onPressed: () => bloc.goToMyPosition(),
        child: const Icon(
          Icons.gps_fixed,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
    );
  }
}
