part of 'connectivity_bloc.dart';

abstract class ConnectivityState extends Equatable {
  const ConnectivityState();
}

class ConnectivityStatus extends ConnectivityState {
    final ConnectivityResult? result;
  const ConnectivityStatus(ConnectivityResult connectivityResult, {this.result});
    
    @override
  List<ConnectivityResult?> get props => [result];
}

class NoConnectivity extends ConnectivityState {
  final ConnectivityResult? result;
  const NoConnectivity(ConnectivityResult connectivityResult, {this.result});

    @override
  List<ConnectivityResult?> get props => [result];
}