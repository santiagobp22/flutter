import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';

part 'connectivity_event.dart';
part 'connectivity_state.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, ConnectivityState> {
  final ConnectivityRepository _connectivityRepository;

  final Stream<ConnectivityResult> _connectivityStream =
      Connectivity().onConnectivityChanged;

  ConnectivityBloc(this._connectivityRepository)
      : super(const ConnectivityStatus(ConnectivityResult.none)) {
    on<ConnectivityEvent>((event, emit) async {
      await emit.forEach(_connectivityStream,
          onData: (ConnectivityResult streamResult) {
            if(streamResult == ConnectivityResult.none) {

              return NoConnectivity(streamResult);
            }
            return ConnectivityStatus(streamResult);
          }
);
    },
    transformer: restartable(),);
  }
}
