import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/models/places.dart';

class HomeMapState extends Equatable {
  final LatLng myLocation;
  final bool loading, gpsEnabled;

  final Map<MarkerId, Marker> markers;
  final Map<PolylineId, Polyline> polylines;
  final Map<PolygonId, Polygon> polygons;

  final Map<String, Place> history;

  final Place arrival, departure;

  HomeMapState({
    required this.myLocation,
    this.loading = true,
    required this.markers,
    required this.polylines,
    required this.polygons,
    this.gpsEnabled = false,
    required this.history,
    required this.arrival,
    required this.departure,
  });

  HomeMapState copyWith({
    LatLng? newLocation,
    bool? isLoading,
    Map<MarkerId, Marker>? newMarkers,
    Map<PolylineId, Polyline>? newPolyLines,
    Map<PolygonId, Polygon>? newPolygons,
    bool? isGPSEnabled,
    Map<String, Place>? newHistory,
    Place? newArrival,
    Place? newDeparture,
  }) {
    return HomeMapState(
      myLocation: newLocation ?? myLocation,
      loading: isLoading ?? loading,
      markers: newMarkers ?? markers,
      polylines: newPolyLines ?? polylines,
      polygons: newPolygons ?? polygons,
      gpsEnabled: isGPSEnabled ?? gpsEnabled,
      history: newHistory ?? history,
      arrival: newArrival ?? arrival,
      departure: newDeparture ?? departure,
    );
  }

  static HomeMapState get initialState => HomeMapState(
        myLocation: LatLng(0, 0),
        markers: {},
        polylines: {},
        polygons: {},
        gpsEnabled: Platform.isIOS,
        history: {},
        arrival: Place(
          id: '',
          title: '',
          vicinity: '',
          position: LatLng(0, 0),
        ),
        departure: Place(
          id: '',
          title: '',
          vicinity: '',
          position: LatLng(0, 0),
        ),
      );

  @override
  List<Object?> get props =>
      [myLocation, loading, markers, gpsEnabled, history, arrival, departure];
}
