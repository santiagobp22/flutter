import 'dart:async';
import 'dart:io' as io;
import 'dart:typed_data';
import 'dart:math' as math;

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_permissions/location_permissions.dart' as lp;
import 'package:wheelio_app/bloc/screens/home/home_map_events.dart';
import 'package:wheelio_app/bloc/screens/home/home_map_state.dart';
import 'package:wheelio_app/models/places.dart';
import 'package:wheelio_app/screens/origin_and_destination_page.dart';
import 'package:wheelio_app/utils/extras.dart';

class HomeMapBloc extends Bloc<HomeMapEvents, HomeMapState> {
  Completer<GoogleMapController> _completer = Completer();
  Future<GoogleMapController> get _mapController async {
    return _completer.future;
  }

  final Completer<Marker> _myPositionMarker = Completer();

  late StreamSubscription<Position> _subscription;

  final lp.LocationPermissions _locationPermissions = lp.LocationPermissions();
  late StreamSubscription _gpsSubscriptionStatus;

  Polyline myRoute = Polyline(
    polylineId: PolylineId('my_route'),
    color: Colors.amber[700] ?? Colors.amber,
    width: 6,
  );

  Polygon myTaps = Polygon(
    polygonId: PolygonId('my_taps'),
    fillColor: Colors.blue,
  );

  @override
  Future<void> close() async {
    _subscription.cancel();
    _gpsSubscriptionStatus.cancel();
    //_mapController.then((value) => value.dispose());
    super.close();
  }

  final LocationSettings localOption = const LocationSettings(
    accuracy: LocationAccuracy.high,
    distanceFilter: 10,
  );

  _init() async {
    _loadCarPin();
    _subscription =
        Geolocator.getPositionStream(locationSettings: localOption).listen(
      (Position position) async {
        LatLng newPosition = LatLng(position.latitude, position.longitude);
        if (position != null) {
          add(
            OnMyLocationUpdate(
              newPosition,
            ),
          );
          CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(
            CameraPosition(
              target: newPosition,
              zoom: 16,
            ),
          );
          (await _mapController).animateCamera(cameraUpdate);
        }
      },
    );
    if (io.Platform.isAndroid) {
      _gpsSubscriptionStatus = _locationPermissions.serviceStatus.listen(
        (status) {
          add(
            OnGPSEnabled(status == lp.ServiceStatus.enabled),
          );
        },
      );
    }
  }

  goToMyPosition() async {
    if (state.myLocation != null) {
      final CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(
        CameraPosition(target: state.myLocation, zoom: 14),
      );
      await (await _mapController).animateCamera(cameraUpdate);
    }
  }

  Future<void> goToPlacePosition(Place place) async {
    await Future.delayed(const Duration(milliseconds: 500));
    add(GoToPlace(place));
    final CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(
        CameraPosition(target: place.position, zoom: 16));
    return (await _mapController).animateCamera(cameraUpdate);
  }

  _loadCarPin() async {
    final Uint8List? bytes = await loadAsset(
      './assets/images/car-pin.png',
      height: 80,
      width: 40,
    );
    final marker = Marker(
      markerId: const MarkerId('my_position_marker'),
      icon: BitmapDescriptor.fromBytes(bytes!),
      anchor: const Offset(0.5, 0.5),
    );
    _myPositionMarker.complete(marker);
  }

  void setMapController(GoogleMapController controller) async {
    if (!_completer.isCompleted) {
      _completer = Completer();
      /*(await _mapController).setMapStyle(
      jsonEncode(mapStyle),
    );*/
      _completer.complete(controller);
    }
  }

  static HomeMapBloc of(BuildContext context) =>
      BlocProvider.of<HomeMapBloc>(context);

  weYouGo(BuildContext context) async {
    final List<Place> history = state.history.values.toList();
    final route = MaterialPageRoute<Place>(
      builder: (_) => OriginalAndDestinationPage(
        departure: state.departure,
        arrival: state.arrival,
        history: history,
        onOriginChanged: (Place origin) {
          add(ConfirmPoint(origin, false));
        },
      ),
      fullscreenDialog: true,
    );
    final Place? itemSelected = await Navigator.push(context, route);
    if (itemSelected != null) {
      await Future.delayed(const Duration(milliseconds: 500));
      add(ConfirmPoint(itemSelected, true));
      CameraUpdate? cameraUpdate;

      if (itemSelected.title != '' &&
          (state.departure.title != '' || state.arrival.title != '')) {
        cameraUpdate = centerMap(
          itemSelected.position,
          state.departure.title == ''
              ? itemSelected.position
              : state.departure.position,
          padding: 100,
        );
      }
      if (cameraUpdate != null) {
        return (await _mapController).animateCamera(cameraUpdate);
      }
    }
  }

  HomeMapBloc()
      : super(
          HomeMapState(
            myLocation: LatLng(0, 0),
            markers: {},
            polylines: {},
            polygons: {},
            history: {},
            arrival: Place(
              id: '',
              title: '',
              vicinity: '',
              position: LatLng(0, 0),
            ),
            departure: Place(
              id: '',
              title: '',
              vicinity: '',
              position: LatLng(0, 0),
            ),
          ),
        ) {
    HomeMapState.initialState;
    _init();

    on<OnMyLocationUpdate>((event, emit) async {
      Place departure = Place(
        id: '',
        title: '',
        vicinity: '',
        position: LatLng(0, 0),
      );

      if (state.departure.id != 'departure') {
        departure = Place(
          id: 'departure',
          title: 'Mi Ubicación',
          position: event.location,
          vicinity: '',
        );
      }

      List<LatLng> points = List<LatLng>.from(myRoute.points);
      points.add(event.location);
      myRoute = myRoute.copyWith(
        pointsParam: points,
      );
      Map<PolylineId, Polyline> polylines = Map.from(
        state.polylines,
      );
      polylines[myRoute.polylineId] = myRoute;
      final markers = Map<MarkerId, Marker>.from(state.markers);
      double rotation = 0;

      LatLng lastPosition = state.myLocation;
      if (lastPosition != null) {
        rotation = getCoordsRotation(event.location, lastPosition);
      }

      final myPositionMarker = (await _myPositionMarker.future).copyWith(
        positionParam: event.location,
        rotationParam: rotation,
      );
      markers[myPositionMarker.markerId] = myPositionMarker;
      return emit(
        state.copyWith(
          isLoading: false,
          newLocation: event.location,
          newPolyLines: polylines,
          newMarkers: markers,
          newDeparture: departure,
        ),
      );
    });

    on<OnMapTap>(
      (event, emit) async {
        final markerId = MarkerId(state.markers.length.toString());
        final info = InfoWindow(
          title: 'MarkerId: ${markerId.value}',
          snippet:
              'LatLng: ${event.location.latitude.toStringAsFixed(2)}, ${event.location.longitude.toStringAsFixed(2)},',
        );

        final bytes = await loadAsset(
          './assets/images/car-pin.png',
          height: 80,
          width: 40,
        );

        /*
        final bytes = await loadImageFromNetwork(
          'https://logowik.com/content/uploads/images/flutter5786.jpg',
          height: 40,
          width: 40,
        );
        */

        final marker = Marker(
          markerId: markerId,
          position: event.location,
          onTap: () {},
          draggable: true,
          onDragEnd: (LatLng newPosition) {},
          infoWindow: info,
          icon: BitmapDescriptor.fromBytes(bytes!),
        );
        final markers = Map<MarkerId, Marker>.from(state.markers);
        markers[markerId] = marker;

        List<LatLng> points = List<LatLng>.from(myTaps.points);
        points.add(event.location);
        myTaps = myTaps.copyWith(
          pointsParam: points,
        );
        Map<PolygonId, Polygon> polygons = Map.from(
          state.polygons,
        );
        polygons[myTaps.polygonId] = myTaps;

        return emit(
          state.copyWith(
            newMarkers: markers,
            newPolygons: polygons,
          ),
        );
      },
    );

    on<OnGPSEnabled>(
      (event, emit) => emit(
        state.copyWith(
          isGPSEnabled: event.enabled,
        ),
      ),
    );

    on<GoToPlace>((event, emit) async {
      final places = Map<String, Place>.from(state.history);

      if (places[event.place.id] == null) {
        places[event.place.id] = event.place;
        final MarkerId markerId = MarkerId('place');
        final Marker marker =
            Marker(markerId: markerId, position: event.place.position);
        final markers = Map.from(state.markers);
        markers[markerId] = marker;
        return emit(
          state.copyWith(
            newHistory: places,
            newArrival: event.place,
          ),
        );
      }
      return emit(
        state.copyWith(
          newArrival: event.place,
        ),
      );
    });

    on<ConfirmPoint>((event, emit) async {
      int? duration;
      RouteData? routeData;
      Map<PolylineId, Polyline>? polylines;

      if (event.isArrival) {
        if (state.departure.title != '') {
          routeData = await createRoute(
            polylines: state.polylines,
            arrival: event.place.position,
            departure: state.departure.position,
          );
          duration = ((routeData!.route.duration / 60.0)).ceil();
        }
      }
      final Uint8List? bytes = await placeToMarker(
        event.place,
        isDeparture: !event.isArrival,
        duration: duration,
      );

      final Marker marker = createMarker(
        id: event.isArrival ? 'arrival' : 'departure',
        position: event.place.position,
        bytes: bytes!,
      );

      final markers = Map<MarkerId, Marker>.from(state.markers);
      markers[marker.markerId] = marker;

      final history = Map<String, Place>.from(state.history);
      history[event.place.id] = event.place;

      if (event.isArrival) {
        polylines = routeData!.polylines;
        return emit(
          state.copyWith(
            newMarkers: markers,
            newArrival: event.place,
            newHistory: history,
            newPolyLines: polylines,
          ),
        );
      }
      return emit(
        state.copyWith(
          newMarkers: markers,
          newDeparture: event.place,
          newHistory: history,
          newPolyLines: polylines,
        ),
      );
    });
  }
}
