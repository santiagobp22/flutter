import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/models/places.dart';
import 'package:wheelio_app/utils/secrets.dart';

class SearchAPI {
  SearchAPI._internal();
  static SearchAPI get instance => SearchAPI._internal();

  final Dio _dio = Dio();
  CancelToken? _cancelToken = CancelToken();

  Future<List<Place>> searchPlace(String query, LatLng at) async {
    _cancelToken = CancelToken();
    try {
      _dio.interceptors.add(
        DioCacheManager(
          CacheConfig(
            baseUrl: 'https://places.ls.hereapi.com/places/v1/autosuggest',
          ),
        ).interceptor,
      );

      final response = await _dio.get(
        'https://places.ls.hereapi.com/places/v1/autosuggest',
        queryParameters: {
          'q': query,
          'apiKey': secretKeyHerePlacesAPI,
          'at': '${at.latitude},${at.longitude}',
        },
        options: buildCacheOptions(
          const Duration(days: 7),
        ),
      );

      if (null != response.headers.value(DIO_CACHE_HEADER_KEY_DATA_SOURCE)) {
        print('XXX: data come from cache');
      } else {
        // data come from net
        print('XXX: data comes from net');
      }

      final List<Place> places = (response.data['results'] as List)
          .where((element) =>
              element['position'] != null && element['title'] != null)
          .map((e) => Place.fromJson(e))
          .toList();
      _cancelToken = null;
      return places;
    } catch (e) {
      print('SearchPlace Failed:$e');
      return [];
    }
  }

  cancel() {
    if (_cancelToken != null && _cancelToken!.isCancelled) {
      _cancelToken!.cancel();
    }
  }
}
