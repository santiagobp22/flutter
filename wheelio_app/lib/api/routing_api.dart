import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wheelio_app/models/route.dart';
import 'package:wheelio_app/utils/secrets.dart';

class RoutingAPI {
  RoutingAPI._interval();

  static RoutingAPI get instance => RoutingAPI._interval();

  final _dio = Dio();

  Future<List<Route>> calculate(LatLng departure, LatLng arrival) async {
    try {
      _dio.interceptors.add(
        DioCacheManager(
          CacheConfig(
            baseUrl: 'https://router.hereapi.com/v8/routes',
          ),
        ).interceptor,
      );

      final response = await _dio.get(
        'https://router.hereapi.com/v8/routes',
        queryParameters: {
          'apiKey': secretKeyHerePlacesAPI,
          'transportMode': 'car',
          'routingMode': 'fast',
          'origin': '${departure.latitude},${departure.longitude}',
          'destination': '${arrival.latitude},${arrival.longitude}',
          'return': 'summary,polyline',
          'alternatives': 1,
        },
        options: buildCacheOptions(
          const Duration(days: 1),
        ),
      );

      return (response.data['routes'] as List)
          .map((e) => Route.fromJson(e))
          .toList();
    } catch (e) {
      print('exception:$e');
    }
    throw UnimplementedError();
  }
}
