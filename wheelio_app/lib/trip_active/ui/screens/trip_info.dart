import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';
import 'package:wheelio_app/Database/bloc/database_bloc.dart';
import 'package:wheelio_app/Database/repository/database_repository.dart';

class TripInfo extends StatefulWidget {
  const TripInfo({Key? key}) : super(key: key);

  @override
  State<TripInfo> createState() => TripInfoState();
}

class TripInfoState extends State<TripInfo> {

  final TextStyle ts = const TextStyle(fontSize: 20, color: Colors.black87);
  final TextStyle tst = const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);

  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      File xd;
    return MaterialApp(
        theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Viaje'),
              centerTitle: true, // appbar text center.
            ),
            body:MultiBlocListener(listeners: [
            BlocListener<ConnectivityBloc, ConnectivityState>(
                listener: (context, state) {

              if (context.read<ConnectivityBloc>().state is NoConnectivity) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showBasicsFlash(context);
              } else if (context.read<ConnectivityBloc>().state is ConnectivityStatus) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showConnectionRestablishedFlash(context);
              }
            }),],
            child: BlocBuilder<DatabaseBloc, DatabaseState>(builder: (context, state) {
              xd = context.read<DatabaseRepositoryImpl>().getUserImage as File;
            return SingleChildScrollView(
                child: Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Colors.white38,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 3,
                              fit: FlexFit.loose,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Inicio:", style: tst),
                                  Text( "xd", style: ts,),
                                  SizedBox.fromSize(size: const Size(1,12),),
                                  Text("Fin:", style: tst),
                                  Text( "xd", style: ts,),
                                ],
                              )),
                          const Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Icon(
                                IconData(0xe3c8, fontFamily: 'MaterialIcons'),
                                size: 50),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Colors.white38,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 3,
                              fit: FlexFit.loose,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Nombre:", style: tst),
                                  Text( "xd", style: ts,),
                                  SizedBox.fromSize(size: const Size(1,12),),
                                  Text("Placa:", style: tst),
                                  Text( "xd", style: ts,),
                                ],
                              )),
                           Flexible(
                            flex:1,
                            fit: FlexFit.tight,
                            child: Image.file(xd)),
                          
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ));}))));
  }
}
