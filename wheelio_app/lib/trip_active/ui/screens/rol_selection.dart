import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';
import 'package:wheelio_app/screens/loading_driver.dart';
import 'package:wheelio_app/trip_search/ui/screens/travel_data_page.dart';
import 'package:wheelio_app/trip_active/bloc/trip_bloc.dart';
import 'package:wheelio_app/trip_active/ui/screens/trip_info.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';


class RolSelection extends StatefulWidget {
  const RolSelection({Key? key}) : super(key: key);
final TextStyle tst = const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);
  @override
  State<RolSelection> createState() => RolSelectionState();
}

class RolSelectionState extends State<RolSelection> {
  List<Trip>? tripsList;
final TextStyle tst = const TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);
  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
      home: Scaffold(
        body: MultiBlocListener(
          listeners: [
            BlocListener<ConnectivityBloc, ConnectivityState>(
                listener: (context, state) {

              if (context.read<ConnectivityBloc>().state is NoConnectivity) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showBasicsFlash(context);
              } else if (context.read<ConnectivityBloc>().state is ConnectivityStatus) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showConnectionRestablishedFlash(context);
              }
            }),
            BlocListener<TripBloc, TripState>(
              listener: (context, state) {
                //Navega si no hay viaje activo y se quiere buscar viaje
                if (state.status == TripStatus.inactiveAsPassenger) {
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => const TravelData()));
                } else if (state.status == TripStatus.inactiveAsDriver) {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => const TravelData()));
                }

                  //Navega si hay un viaje activo como pasajero

                if (state.status == TripStatus.activeAsPassenger) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => LoadingDriver(
                                end: '',
                                start: '',
                              )),
                      (Route<dynamic> route) => false);
                      //Navega si hay un viaje activo como conductor
                } else if (state.status == TripStatus.activeAsDriver) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => const TripInfo()),
                      (Route<dynamic> route) => false);

                }
              },
            )
          ],
          child: BlocBuilder<TripBloc, TripState>(builder: (context, state) {
            //No hay viaje activo

              return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 75.0),
                  child: Flex(direction: Axis.vertical,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Ink(
                          decoration: const ShapeDecoration(
                            color: Colors.lightBlue,         
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            iconSize: 200,
                            icon: const Icon(Icons.airport_shuttle_outlined),
                            color: Colors.white,
                            onPressed: () {
                              context
                                  .read<TripBloc>()
                                  .add(TripCheckedAsDriver());
                            },
                          ),
                        ),
                        Text('Conductor',
                        style: tst,
                        ),
                        SizedBox(height: 50),
                        Ink(
                          decoration: const ShapeDecoration(
                            color: Colors.lightBlue,
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            iconSize: 200,
                            icon: const Icon(Icons.person),
                            color: Colors.white,
                            onPressed: () {
                              context
                                  .read<TripBloc>()
                                  .add(TripCheckedAsPassenger());
                            },
                          ),
                        ),
                        Text('Pasajero',
                        style: tst,),
                      ]));
            }
          ),
        ),
      ),
    );
  }
}
