import 'package:wheelio_app/trip_search/models/trip.dart';

import '../services/trip_service.dart';

abstract class TripRepository {
  Future<Trip> getTrip();
  Future<void> updateTripPassenger();
}

class TripRepositoryImpl implements TripRepository {
  TripService service = TripService();

  @override
  Future<Trip> getTrip() {
    return service.getTrip();
  }

  @override 
  Future<void> updateTripPassenger() async{
  }
}
