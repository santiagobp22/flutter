import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wheelio_app/trip_active/repository/trip_repository.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';

part 'trip_event.dart';
part 'trip_state.dart';

class TripBloc extends Bloc<TripEvent, TripState> {
  TripBloc({
    required TripRepository tripRepository,
    required Trip? initialTrip,
  })  : _tripRepository = tripRepository,
        super(
          TripState(
            initialTrip: initialTrip,
          ),
        ) {
    on<TripPassengerAdded>(_onPassengerAdded);
    on<TripCheckedAsDriver>(_onCheckedAsDriver);
    on<TripCheckedAsPassenger>(_onCheckedAsPassenger);
  }

  final TripRepository _tripRepository;

  Future<void> _onPassengerAdded(
    TripPassengerAdded event,
    Emitter<TripState> emit,
  ) async {
    try {
      await _tripRepository.getTrip();
      emit(state.copyWith(status: TripStatus.activeAsDriver));
    } catch (e) {
      emit(state.copyWith(status: TripStatus.inactiveAsDriver));
    }
  }

  _onCheckedAsDriver(
    TripCheckedAsDriver event,
    Emitter<TripState> emit,
  ) async {
    try {
      var trip = await _tripRepository.getTrip();
      emit(state.copyWith(status: TripStatus.inactiveAsDriver, initialTrip: trip, isDriver: true));
    } catch (e) {
      emit(state.copyWith(status: TripStatus.inactiveAsDriver));
    }
  }

    _onCheckedAsPassenger(
    TripCheckedAsPassenger event,
    Emitter<TripState> emit,
  ) async {
    try {
      var trip = await _tripRepository.getTrip();
      emit(state.copyWith(status: TripStatus.inactiveAsPassenger, initialTrip: trip, isDriver: false));
    } catch (e) {
      emit(state.copyWith(status: TripStatus.inactiveAsPassenger));
    }
  }
}
