part of 'trip_bloc.dart';

enum TripStatus { initial, activeAsDriver, activeAsPassenger, inactiveAsDriver, inactiveAsPassenger }

class TripState extends Equatable {
  const TripState({
    this.status = TripStatus.initial,
    this.initialTrip,
    this.isDriver = false,
  });

  final TripStatus status;
  final Trip? initialTrip;
  final bool isDriver;

   TripState copyWith({
    TripStatus? status,
    Trip? initialTrip,
    bool? isDriver,
  }) {
    return TripState(
      status: status ?? this.status,
      initialTrip: initialTrip ?? this.initialTrip,
      isDriver: isDriver ?? this.isDriver,
    );
  }

  @override
  List<Object?> get props => [status, initialTrip, isDriver];
}



