part of 'trip_bloc.dart';

abstract class TripEvent extends Equatable {
  const TripEvent();

  @override
  List<Object> get props => [];
}

class TripCreated extends TripEvent {
  @override
  List<Object> get props => [];
}

class TripPassengerAdded extends TripEvent {
  @override
  List<Object> get props => [];
}

class TripCheckedAsPassenger extends TripEvent {
  @override
  List<Object> get props => [];
}

class TripCheckedAsDriver extends TripEvent {
  @override
  List<Object> get props => [];
}


class TripFinished extends TripEvent {
  @override
  List<Object> get props => [];
}