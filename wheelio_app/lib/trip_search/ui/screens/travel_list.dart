import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wheelio_app/Connectivity/bloc/connectivity_bloc.dart';
import 'package:wheelio_app/Connectivity/repository/connectivity_repository.dart';
import 'package:wheelio_app/Database/bloc/database_bloc.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';
import 'package:wheelio_app/trip_search/ui/widgets/trip_card.dart';


class TravelList extends StatefulWidget {
  const TravelList({Key? key}) : super(key: key);

  @override
  State<TravelList> createState() => TravelListState();
}

class TravelListState extends State<TravelList> {


  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Trip> tripsList = [];
    return MaterialApp(
        title: 'Viajes disponibles',
        theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
        home: Scaffold(
          appBar:AppBar(
              title: const Text('Oferta de viajes'),
              centerTitle: true, // appbar text center.
            ),
          body: MultiBlocListener(listeners: [
            BlocListener<ConnectivityBloc, ConnectivityState>(
                listener: (context, state) {
              if (context.read<ConnectivityBloc>().state is NoConnectivity) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showBasicsFlash(context);
              } else if (context.read<ConnectivityBloc>().state is ConnectivityStatus) {
                context
                    .read<ConnectivityRepositoryImpl>()
                    .showConnectionRestablishedFlash(context);
              }
            }),],
            child:BlocBuilder<DatabaseBloc, DatabaseState>(
              builder: (context, state) {
                    
                if (state is DatabaseInitial) {
                  context.read<DatabaseBloc>().add(DatabaseFetched(tripsList));
                  return const Center(child: CircularProgressIndicator());
                } else if (state is DatabaseSuccess) {
                  if (state.tripList.isEmpty) {
                    return const Center(
                      child: Text("There are no trips available"),
                    );
                  } else {
                    return ListView.builder(
                        itemCount: state.tripList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TripCard(state.tripList[index]);
                        });
                  }
                } else {
                  return const Center(child: CircularProgressIndicator());
                }
              },
            )
          ),
        ));
  }
}
