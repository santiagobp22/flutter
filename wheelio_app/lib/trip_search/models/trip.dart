import 'package:cloud_firestore/cloud_firestore.dart';


class Trip {
  int? id;
  String? otherAddress;
  String? otherAddressName;
  int? pricePerSeat;
  int? seats;
  Timestamp? submitTime;
  bool? toU;
  int tripDateTime;
  

  Trip(
    {
  this.id,
  this.pricePerSeat,
  this.seats,
  this.submitTime,
  this.toU,
  required this.tripDateTime,
  this.otherAddress,
  this.otherAddressName
    }
  );

  factory Trip.fromJson(Map<String, dynamic> json) {
    return Trip(
      id:  json['id'],
      pricePerSeat:  json['pricePerSeat'],
      seats:  json['seats'],
      submitTime:  json['submitTime'],
      toU:  json['toU'],
      tripDateTime:  json['tripDateTime'],
      otherAddressName: json['otherAddressName'],
    );
  }
  
  Trip.fromSnapshot(snapshot)
  :   otherAddress = snapshot.data()['otherAddress'],
  pricePerSeat =  snapshot.data()['pricePerSeat'],
      seats =  snapshot.data()['seats'],
      submitTime =  snapshot.data()['submitTime'],
      toU =  snapshot.data()['toU'],
      tripDateTime =  snapshot.data()['tripDateTime'],
      otherAddressName = snapshot.data()['otherAddressName'];
}