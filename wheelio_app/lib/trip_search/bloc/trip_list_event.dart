part of 'trip_list_bloc.dart';

abstract class TripListEvent extends Equatable{
  const TripListEvent();
}

class TripListStarted extends TripListEvent {
  @override
  List<Object> get props => [];
}

class TripPreferencesSaved extends TripListEvent {
  final bool? goingHome;
  final String price;
  final String start;
  final String end;

  const TripPreferencesSaved(this.goingHome, this.price, this.start, this.end);
  @override
  List<Object> get props => [];
}

class TripPreferencesRetrived extends TripListEvent {
  @override
  List<Object> get props => [];
}