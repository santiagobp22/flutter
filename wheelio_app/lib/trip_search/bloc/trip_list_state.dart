part of 'trip_list_bloc.dart';

enum TripListStatus { initial, noDataConnectivity, dataConnectivity, noDataNoConnectivity, dataNoConnecticity }

abstract class TripListState extends Equatable{
  const TripListState();
}

class TripListInitial extends TripListState {
    @override
  List<Object> get props => [];
}

class TripPreferencesOn extends TripListState {
  final List<String>? preferences;
  const TripPreferencesOn(this.preferences);
    @override
  List<Object> get props => [];
}

class TripListDataConnectivity extends TripListState{
  final List<Trip> trips;
  const TripListDataConnectivity(this.trips);

  @override
  List<Object> get props => [];
}

class TripListNoDataNoConnectivity extends TripListState{
  @override
  List<Object> get props => [];
}

class TripListDataNoConnectivity extends TripListState{
  final List<Trip> trips;
  const TripListDataNoConnectivity(this.trips);
  @override
  List<Object> get props => [];
}