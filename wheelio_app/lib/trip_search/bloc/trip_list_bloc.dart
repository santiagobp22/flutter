import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';
import 'package:wheelio_app/trip_search/repository/trip_list_repository.dart';


part 'trip_list_event.dart';
part 'trip_list_state.dart';

class TripListBloc extends Bloc<TripListEvent, TripListState> {
  final TripListRepository _tripListRepository;

  TripListBloc(this._tripListRepository)
      : super(TripListInitial()) {
    on<TripListEvent> (fetchTripList);
    on<TripPreferencesSaved> (saveTripPreferences);
    on<TripPreferencesRetrived> (getTripPreferences);
      }

      saveTripPreferences (TripPreferencesSaved event, Emitter<TripListState> emit) async {
        List<String> preferenceList = [event.goingHome.toString(), event.price, event.start, event.end];
        await _tripListRepository.updateTripPreferencesLocalStorage(preferenceList);
      }

      getTripPreferences (TripPreferencesRetrived event, Emitter<TripListState> emit) async {
        emit(TripPreferencesOn(await _tripListRepository.getTripPreferencesLocalStorage()));
      }








  fetchTripList(TripListEvent event, Emitter<TripListState> emit) async {

  }


}
