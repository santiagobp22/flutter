import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class TripListService {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  final storage = const FlutterSecureStorage();

  List<Trip> tripList = [];

  Future<List<Trip>> getTripList() async {
    var data = await _db
        .collection('offers')
        .orderBy('tripDateTime', descending: true)
        .get();
    return List.from(data.docs.map((doc) => Trip.fromSnapshot(doc)));
  }

//Secure local storage logic

  Future<void> updateTripPreferencesLocalStorage(List<String> preferences) async {
      final prefs =  await SharedPreferences.getInstance();
    await prefs.setStringList('preferences', preferences);
  }

  Future<List<String>?> getTripPreferencesLocalStorage() async {
  final prefs =  await SharedPreferences.getInstance();
    final List<String>? items = prefs.getStringList('preferences');
    return items;
  }



  Future<List<Trip>> getTripListLocalStorage() async {
    await readTripLocalStorage();
    return tripList;
  }

    Future<void> readTripLocalStorage() async {
    String? stringList = await storage.read(key: 'listoftrips');
    tripList = List<Trip>.from(jsonDecode(stringList!));
    }

  Future<void> updateTripListLocalStorage(List<Trip> pTripList) async {
    await storage.delete(key: 'listoftrips');
    await storage.write(key: 'listoftrips', value: jsonEncode(pTripList));
  }
}
