import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file/src/interface/file.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_cache_manager_firebase/flutter_cache_manager_firebase.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';

class DatabaseService {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final storage = const FlutterSecureStorage();
  late final prefs = SharedPreferences.getInstance();

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
  List<Trip> tripList = [];

  Future<List<Trip>> getTripList() async {
    try {
      var data = await _db
          .collection('offers')
          .orderBy('tripDateTime', descending: true)
          .get();
      tripList = List.from(data.docs.map((doc) => Trip.fromSnapshot(doc)));

      //await prefs.setString('trips', jsonEncode(tripList));
      //await storage.write(key: 'trips', value: jsonEncode(tripListString), aOptions: _getAndroidOptions());

    } catch (e) {
      //String? stringList = await storage.read(key: 'trips');
      //final String? action = prefs.getString('action');
      //tripList = List<Trip>.from(jsonDecode(stringList!));
      var file = await FirebaseCacheManager().getSingleFile(
          'https://firestore.googleapis.com/v1/projects/wheelsdemo-3c71f/databases/(default)/documents/offers');

    }

    return tripList;
  }

  Future<File> getUserImage() async {
    var file = await FirebaseCacheManager().getSingleFile(
        'gs://wheelsdemo-3c71f.appspot.com/kisspng-trucker-duck-truck-driver-natural-rubber-bathtub-rubber-duck-5b10fbc4d91a42.7850694515278396848893.jpg');
    return file;
  }

  //Secure local storage logic
  Future<List<Trip>> getTripListLocalStorage() async {
    await readTripLocalStorage();
    return tripList;
  }

  Future<void> readTripLocalStorage() async {
    String? stringList = await storage.read(key: 'listoftrips');
    tripList = List<Trip>.from(jsonDecode(stringList!));
  }

  Future<void> updateTripListLocalStorage(List<Trip> pTripList) async {
    await storage.delete(key: 'listoftrips');
    await storage.write(key: 'listoftrips', value: jsonEncode(pTripList));
  }
}
