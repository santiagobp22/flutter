
import 'package:flutter_cache_manager/file.dart';

import '../services/database_service.dart';

abstract class DatabaseRepository {
  Future getTripList();
  Future<File> getUserImage();
}

class DatabaseRepositoryImpl implements DatabaseRepository {
  DatabaseService service = DatabaseService();

  @override
  Future getTripList() {
    return service.getTripList();
  }

  @override
  Future<File> getUserImage()  {
    return service.getUserImage();
  }
}