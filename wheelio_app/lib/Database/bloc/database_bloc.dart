import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:wheelio_app/trip_search/bloc/trip_list_bloc.dart';
import 'package:wheelio_app/trip_search/models/trip.dart';

import '../repository/database_repository.dart';
part 'database_event.dart';
part 'database_state.dart';

class DatabaseBloc extends Bloc<DatabaseEvent, DatabaseState> {
  final DatabaseRepository _databaseRepository;
  DatabaseBloc(this._databaseRepository) : super(DatabaseInitial()) {
    on<DatabaseStarted>((event, emit) => null);
    on<DatabaseFetched>(fetchTripList);
    on<UserImageFetched>(getUserImage);
  }


  fetchTripList(DatabaseFetched event, Emitter<DatabaseState> emit) async {
    
      List<Trip> tripList = await _databaseRepository.getTripList();

      emit(DatabaseSuccess(tripList));
  }

    getUserImage(UserImageFetched event, Emitter<DatabaseState> emit) async {
    
      File photo = await _databaseRepository.getUserImage();
  }
}