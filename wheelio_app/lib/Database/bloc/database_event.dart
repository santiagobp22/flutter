part of 'database_bloc.dart';



abstract class DatabaseEvent extends Equatable {
  const DatabaseEvent();

  @override
  List<Object?> get props => [];
}

class DatabaseStarted extends DatabaseEvent {
    @override
  List<Object?> get props => [];
}

class DatabaseFetched extends DatabaseEvent {
  final List<Object>? tripList;
  const DatabaseFetched(this.tripList);
  @override
  List<Object?> get props => [tripList];
}

class UserImageFetched extends DatabaseEvent {}
