part of 'database_bloc.dart';


abstract class DatabaseState extends Equatable {
  const DatabaseState();
  
  @override
  List<Object?> get props => [];
}

class DatabaseInitial extends DatabaseState {}

class DatabaseSuccess extends DatabaseState {
  final List<Trip> tripList;
  const DatabaseSuccess(this.tripList);

    @override
  List<Object?> get props => [tripList];
}

class DatabaseError extends DatabaseState {
      @override
  List<Object?> get props => [];
}