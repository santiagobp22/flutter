import 'package:wheelio_app/Authentication/service/authentication_service.dart';
import 'package:wheelio_app/Database/services/database_service.dart';

abstract class AuthenticationRepository {

  Future<void> performLogin(String provider, List<String> scopes,
      Map<String, String> parameters);
  Future<void> performLink(String provider, List<String> scopes,
      Map<String, String> parameters);
}

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  AuthenticationService service = AuthenticationService();
  DatabaseService dbService = DatabaseService();

  @override
  Future<void> performLink(String provider, List<String> scopes, Map<String, String> parameters) async {
    await service.performLink(provider, scopes, parameters);
  }

  @override
  Future<void> performLogin(String provider, List<String> scopes, Map<String, String> parameters) async {
    await service.performLogin(provider, scopes, parameters);
  }
}

