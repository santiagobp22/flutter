import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wheelio_app/Authentication/bloc/authentication_bloc.dart';
import 'package:wheelio_app/trip_active/ui/screens/rol_selection.dart';


class HomeLogin extends StatelessWidget {
  const HomeLogin({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {

    return BlocConsumer<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
              if (state is AuthenticationSuccess) {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => const RolSelection()),
                    (Route<dynamic> route) => false);
              }
            },
      builder: (context, state) {
        if (state is AuthenticationInitial || state is AuthenticationFailure)
        {
          return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFFFFE45C)),
      home: Scaffold(
          body: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 75.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Image(
                        image: AssetImage('assets/images/Logo.PNG')),
                        SizedBox(height: 10),
                      const Text('Worry about studying, we care about your transportation.',
                      style: TextStyle(fontSize: 20, color: Colors.black87),
                      textAlign: TextAlign.center,),
                      SizedBox(height: 50),
                      SizedBox(
                               width: 170,
                              height: 50, 
                      child: ElevatedButton(
                        onPressed: (() => context.read<AuthenticationBloc>().add(AuthenticationStarted())),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF00468B)),
                          ), child: const Text("Log in"),
                        ),

                      ),
                    ],
                  ),
                ),
              ));
        }
        return Container();
      }
    );

  }}